package com.example.samplemvi.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.samplemvi.utils.Constants
import com.example.samplemvi.R
import com.example.samplemvi.databinding.DetailFragmentBinding
import kotlinx.coroutines.flow.collect

class DetailFragment : Fragment(R.layout.detail_fragment) {
    private var _binding: DetailFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MainViewModel by activityViewModels()

    private val args: DetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val id = args.id
        viewModel.setEvent(MainContract.Event.OnLoadMovieClicked(id))

        binding.btnRetry.setOnClickListener {
            viewModel.setEvent(MainContract.Event.OnBtnRetryClicked(id))
        }

        initObservers()
    }

    private fun initObservers() {
        lifecycleScope.launchWhenStarted {
            viewModel.uiState.collect {
                val isError = it.errorDetail != null || it.errorDetailRes != null
                binding.apply {
                    progress.isVisible = it.isLoadingDetail
                    viewContent.visibility = if (isError) View.GONE else View.VISIBLE
                    viewError.visibility = if (isError) View.VISIBLE else View.GONE
                    errorTxt.text = if (it.errorDetailRes != null) getString(it.errorDetailRes) else it.errorDetail
                }
                it.movieDetail?.let { movie ->
                    movie.poster_path?.let {
                        Glide.with(requireContext())
                            .load(Constants.DEFAULT_IMAGE_URL + it)
                            .into(binding.poster)
                    }
                    Glide.with(requireContext())
                        .load(Constants.FULL_IMAGE_URL + movie.backdrop_path)
                        .into(binding.banner)
                    binding.apply {
                        title.text = movie.title
                        voteAverage.text = getString(R.string.vote_average, movie.vote_average.toString())
                        voteCount.text = getString(R.string.vote_count, movie.vote_count.toString())
                        releaseDate.text = getString(R.string.release_date, movie.release_date)
                        shortDescriptions.text = movie.overview
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.setEvent(MainContract.Event.OnHomeBackClicked)
        _binding = null
    }
}